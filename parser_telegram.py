"""
Из Telegram экспортируем json файл - result.json
Складываем любое кол-во json файлов в подпапки INPUT
Скрипт перебирает все файлы из подпапок и создает xlsx файл. На каждом отдельном листе будет содержаться книга контактов
"""
import re
import json
import openpyxl as xlsx
import os.path
from openpyxl.styles import Font, Color, colors

JSON_FILE = 'result.json'               #имя json файла
EXCEL_FILE = 'contacts_telegram.xlsx'   #имя конечного excel файла с контактами
DIR_PREFIX_USERS = 'INPUT/'             #директория с подпапками пользовательских json файлов
DIR_PREFIX_OUTPUTFILE = ''              #директория с конечным excel файлом

# функция удаляет файл по указанному имени в указанной директории
def file_deleter(filename, filepath=''):
    if filepath and (not os.path.isdir(re.sub(r'[/, \, //, \\]', '', filepath))):
        os.mkdir(re.sub(r'[/, \, //, \\]', '', filepath))
    if os.path.isfile(filepath + filename):
        os.remove(filepath + filename)

# функция открывает/создает excel_file возвращает объект openpyxl workbook и добавляет лист с именем sheetname
def excel_opener(excel_file, sheetname='SHEET1', filepath=''):
    if not os.path.isfile(filepath + excel_file):
        excel_file = xlsx.Workbook()
        excel_file.active.title = sheetname
    else:
        excel_file = xlsx.load_workbook(filepath + excel_file)
        excel_file.create_sheet(sheetname)
    return excel_file


if __name__ == '__main__':
    # 1. зачищаем файл экспорта контактов
    file_deleter(EXCEL_FILE, DIR_PREFIX_OUTPUTFILE)

    # 2. перебираем папки с json файлами
    users_dirs = os.listdir('./{}'.format(DIR_PREFIX_USERS))
    for users in range(len(users_dirs)):
        # считываем JSON файл telegram и выгружаем список в contacts_list
        with open('{}/{}/{}'.format(DIR_PREFIX_USERS, users_dirs[users], JSON_FILE), 'r',  encoding='utf8') as file:
            buffer = json.load(file)
        contacts_list = buffer['contacts'].get('list')
        personal_info = buffer['personal_information']

        #3. экспортируем все в excel файлик
        sheetname = '{}_ID-{}'.format(personal_info['first_name'], personal_info['user_id'])
        excel_file = excel_opener(EXCEL_FILE, sheetname, DIR_PREFIX_OUTPUTFILE)
        excel_worksheet = excel_file[sheetname]

        # 4. парсим json и заполняем наш xlsx файл
        for contact in range(len(contacts_list)):
            excel_worksheet[f'A{str(contact + 5)}'] = contacts_list[contact]['first_name']
            excel_worksheet[f'B{str(contact + 5)}'] = contacts_list[contact]['phone_number']

        # разметка заголовков страницы excel
        excel_worksheet['A1'] = 'НАШ ТЕЛЕГРАМ ID: ' + str(personal_info['user_id'])
        excel_worksheet['A2'] = 'НАШЕ ИМЯ: ' + str(personal_info['first_name']) + ' ' + str(personal_info['last_name'])
        excel_worksheet['A3'] = 'НАШ НОМЕР ТЕЛЕФОНА: ' + str(personal_info['phone_number'])
        excel_worksheet['A4'] = 'ИМЯ КЛИЕНТА'
        excel_worksheet['B4'] = 'ТЕЛЕФОН КЛИЕНТА'

        # форматирование ячеек
        excel_worksheet['A1'].font = Font(color=colors.BLUE, bold=True)
        excel_worksheet['A2'].font = Font(color=colors.BLUE, bold=True)
        excel_worksheet['A3'].font = Font(color=colors.BLUE, bold=True)
        excel_worksheet['A4'].font = Font(bold=True)
        excel_worksheet['B4'].font = Font(bold=True)
        excel_worksheet.column_dimensions['A'].width = 42
        excel_worksheet.column_dimensions['B'].width = 16

        # 5. сохранение конечного excel файла
        excel_file.save(DIR_PREFIX_OUTPUTFILE + EXCEL_FILE)